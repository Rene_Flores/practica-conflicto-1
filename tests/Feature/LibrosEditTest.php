<?php

namespace Tests\Feature;

use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;
use App\Models\Libro;

class LibrosEditTest extends TestCase

{
    use RefreshDatabase;

    public function test_libros_update_success(): void
    {
        $libro = Libro::findOrFail(1);
    
        $libroData = [
            'titulo' => 'aaaaaaa',
            'editorial' => 'bbbbbbbb',
        ];
    
        $response = $this->put(route('libros.update', $libro->id), $libroData);
    
        $response->assertStatus(302);
    
        $response->assertRedirect(route('libros.index'));
    
        $response->assertSessionHas('msn_success');
    
        $cursoData['id'] = $libro->id;
    
        $this->assertDatabaseHas('libros', $cursoData);
    
        $this->assertDatabaseMissing('libros', [
            'id' => $libro->id,
            'titulo' => $libro->titulo,
            'editorial' => $libro->editorial,
        ]);
    }
    
    public function test_libros_update_validation(): void
{
    $libro = Libro::findOrFail(1);

    $libroData = [
        'titulo' => '',
        'editorial' => '',
    ];

    $response = $this->put(route('libros.update', $libro->id), $libroData);

    $response->assertStatus(302);

    $response->assertSessionHasErrors([
        'titulo',
        'editorial',
    ]);
}

public function test_libros_update_exception(): void
{
    $libro = Libro::findOrFail(1);

    $nombre = '';
    for ($i = 0; $i < 255; $i++) {
        $nombre .= 'Curso Largo ';
    }

    $response = $this->put(route('libros.update', $libro->id), [
        'titulo' => $nombre,
        'editorial' => 'nombre de la editorial',
    ]);

    $response->assertStatus(302);

    $response->assertSessionHas('msn_error');
}
}
