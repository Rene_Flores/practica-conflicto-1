@extends('layout')    
@section('content')

<div class="container">
    
<table class="table" border="1">
        <thead>
            <tr>
                <th>ID</th>
                <th>Titulo</th>
                <th>Editorial</th>
                <th></th>
            </tr>
        </thead>
        <tbody>
            @foreach($libros as $libro)
            <tr>
                <td>{{ $libro->id }}</td>
                <td>{{ $libro->titulo }}</td>
                <td>{{ $libro->editorial }}</td>
                <td>
                    <a href="{{ route('libros.edit', ['idlibros' => $libro->id]) }}" class="btn btn-primary">Editar</a>
                </td>
            </tr>
            @endforeach
        </tbody>
    </table>
</div>

@endsection