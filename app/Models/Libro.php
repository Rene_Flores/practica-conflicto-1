<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Libro extends Model
{
    use HasFactory;

    use HasFactory;
    protected $table = 'libros'; // Nombre de la tabla en la base de datos
    protected $fillable = ['id','titulo', 'editorial'];
    
}
